using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiplayerGame
{
    public class Manager : MonoBehaviour
    {
        void Start()
        {
            Server.ServerController.Instance.ConnectToServer(Resources.ServerIp, Resources.ServerPort);
        }

    }

}
