using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;


namespace MultiplayerGame.Server
{
    public class ServerController
    {


        private static ServerController instance;

        public static ServerController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ServerController();
                }

                return instance;
            }
        }

        private TCPController TCPController;

        public void ConnectToServer(string ip, int port)
        {
            this.TCPController = new TCPController(ip, port);

            this.TCPController.Connect(onSuccess: OnSucces, onDataReceived: OnDataRecieved, onDisconnected: OnDisconnect, onError: OnErorr);
        }

        private void OnSucces()
        {
            Debug.Log("We are connected to ther Server!!!!!!");
        }
        private void OnDataRecieved(byte[] data)
        {

        }

        private void OnDisconnect()
        {
            Debug.LogError("The server has disconnected!");
        }

        private void OnErorr(string message)
        {

        }

    }
}
