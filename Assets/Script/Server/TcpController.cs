﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace MultiplayerGame.Server
{
    public class TCPController
    {
        #region Events

        private event Action onDisconnected;
        private Action<byte[]> onDataReceived;
        private int retryWFRDCount;

        #endregion

        #region Private Variables

        private readonly string serverIP = "";
        private readonly int serverPortNumber = 0;
        private readonly int connectTimeout = 10;
        private readonly int retryWFRDCountMax = 3;
        private readonly int receiveBufferSize = 4096;
        private readonly int sendBufferSize = 4096;
        private TcpClient tcpClient;

        #endregion

        #region Constructors

        public TCPController(string serverIP, int serverPortNumber)
        {
            this.serverIP = serverIP;
            this.serverPortNumber = serverPortNumber;
        }

        #endregion

        #region Properties

        public float ConnectTimeout
        {
            get { return this.connectTimeout; }
        }

        #endregion

        #region Methods

        public async void Connect(Action onSuccess, Action<string> onError, Action<byte[]> onDataReceived, Action onDisconnected)
        {
            try
            {
                this.onDataReceived = onDataReceived;
                this.onDisconnected = onDisconnected;

                this.tcpClient = GetNewTCPClient();
                await this.tcpClient.ConnectAsync(this.serverIP, this.serverPortNumber);
                if (this.tcpClient.Client.Connected)
                {
                    onSuccess();
                    this.WaitForReceiveData();
                }
                else
                {
                    onError("");
                    this.Disconnect();
                }
            }
            catch (Exception e)
            {
                this.Disconnect();
                UnityEngine.Debug.Log(e.Message);
                onError(e.StackTrace);
            }
        }

        public void Disconnect()
        {
            this.tcpClient.Close();
            onDisconnected?.Invoke();
        }

        private TcpClient GetNewTCPClient()
        {
            TcpClient tcpClient = new TcpClient()
            {
                ReceiveBufferSize = this.receiveBufferSize,
                SendBufferSize = this.sendBufferSize,
                ReceiveTimeout = connectTimeout,
                SendTimeout = connectTimeout,
            };
            return tcpClient;
        }

        private async void WaitForReceiveData()
        {
            try
            {
                NetworkStream readStream = this.tcpClient.GetStream();
                byte[] receivedData = new byte[this.tcpClient.ReceiveBufferSize];
                var receivedDataLength = await readStream.ReadAsync(buffer: receivedData, offset: 0, count: receivedData.Length);
                this.onDataReceived(receivedData.Skip(0).Take(receivedDataLength).ToArray());
                this.WaitForReceiveData();
                retryWFRDCount = 0;
            }
            catch (Exception e)
            {
                retryWFRDCount++;
                if (retryWFRDCount >= retryWFRDCountMax)
                {
                    retryWFRDCount = 0;
                    Disconnect();
                }
                else
                {
                    this.WaitForReceiveData();
                }
                UnityEngine.Debug.Log(e.Message + " \n" + e.StackTrace);
            }
        }

        public async System.Threading.Tasks.Task SendData(byte[] data, Action<int, string> onError)
        {
            try
            {
                NetworkStream writeStream = this.tcpClient.GetStream();
                await writeStream.WriteAsync(data, 0, data.Length);
            }
            catch (Exception e)
            {
                onError(0, e.StackTrace);
                UnityEngine.Debug.Log(e.StackTrace);
            }
        }

        #endregion
    }
}
